# UCSD

<!-- This directory uses the Neural cleanse based method for round 1 data. It seems the method does not work well. -->

This repo includes the codes for TrojAI project Round5.

Best  CrossEntropyLoss: 0.3320

#how to run:

python round5_7_stable.py --model_filepath ./id-00000016/model.pt  --cls_token_is_first --tokenizer_filepath ./tokenizers/BERT-bert-base-uncased.pt --embedding_filepath ./embeddings/BERT-bert-base-uncased.pt --result_filepath ./output.txt --scratch_dirpath ./scratch/ --examples_dirpath ./id-00000016/clean_example_data 

import numpy as np
import os
import csv
import skimage.io
import torch
​
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
​
def img_transform(img):
    # same transformation that the NIST example applies, torchvision ones should be the same 
    # but didn't work on round 1
    h, w, c = img.shape
    #resize to 224x224
    dx = int((w - 224) / 2)
    dy = int((w - 224) / 2)
    img = img[dy:dy+224, dx:dx+224, :]
​
    #cxhxw + normalization
    img = np.transpose(img, (2, 0, 1))
    img = np.expand_dims(img, 0)
    img = img - np.min(img)
    img = img / np.max(img)
​
    return torch.FloatTensor(img)
​
​
def load_data(example_path='example_data', img_format='png'):
    data = [os.path.join(example_path, ex) for ex in os.listdir(example_path) if ex.endswith(img_format)]
    labels = [int(ex.split('_')[1]) for ex in os.listdir(example_path) if ex.endswith(img_format)]
    batch = torch.empty(len(data),3,224,224)
    for i,x in enumerate(data):
        img = skimage.io.imread(x)
        #switch to torchvision transforms when the channel thing is fixed
        batch[i] = img_transform(img)
    
    return batch, torch.LongTensor(labels)
​
def data_split(data, labels):
    # assumes same # of examples for each class--this is true for r2 at least
    u,c = torch.unique(labels, return_counts=True)
    X = torch.empty(len(u),c[0].item(), 3,224,224)
    for i,label in enumerate(u):
        X[i] = data[labels==label]
    return X
        
​
def grad_stack(model, data, j):
    '''
    returns a stack of gradients of the jth output of model(data)
    w.r.t. each datapoint
    '''
    output = model(data)[:,j]
    grad = torch.autograd.grad(outputs=output, inputs=data, 
         grad_outputs = torch.ones_like(output))
    
    return grad[0].flatten(start_dim=1, end_dim=3)
​
​
def label_shift(model, data, svec, epsilon):
    '''
    takes the singular vector for each class as a Trojan patch candidate, adds each to a batch of true data
    points and looks at the shift in classification for various scalings.  
    For trojaned models these are very effective and switching classifications to the target class,
    clean models are less responsive.  The response for epsilon < 5 and epsilon >20 are particularly distinguishing
    '''
    C = model(data[0:1]).shape[1]
    n = len(data)
    pct_shift = torch.zeros(C,C,len(epsilon)).to(device)
    for i,s in enumerate(svec):
        v = s.reshape(3,224,224).to(device) 
        for j,e in enumerate(epsilon):
            pred = model(data + e*v).argmax(dim=1)
            unique, counts = torch.unique(pred, return_counts=True)
            pct_shift[i, unique.long(), j] = counts.float() / n
            
    return pct_shift
​
def is_trojan(file):
    with open(os.path.join(file,'ground_truth.csv'),newline='') as file:
        reader = csv.reader(file, delimiter=',')
        trojaned = bool(next(iter(reader))[0])
    return trojaned
​
def is_poly(file):
    return ('trigger.png' in os.listdir(file))
​
def main(model_filepath, result_filepath, scratch_dirpath, examples_dirpath, example_img_format='png'):
    #chosen by experiment
    epsilon = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 5.0, 7.5, 10.0, 12.5, 15.0, 17.5,
                                  20.0, 25.0, 50.0]
    
    model = torch.load(model_filepath).to(device)
    data, labels = load_data(example_path = examples_dirpath)
    X = data_split(data,labels)
    C = len(torch.unique(labels))
    n = len(X[0])
    
    gradients = torch.empty(n, 150528)
    svecs = torch.empty(C,150528)
    pct_shift = torch.empty(C,C,C, len(epsilon))
    
    for j in range(C):
        Z = X[j].clone().requires_grad_().to(device)
        for k in range(C):
            gradients = grad_stack(model, Z, k)
            temp,_,_ = torch.svd(gradients.cpu().T)
            svecs[k] = temp[:, 0]
        pct_shift[j] = label_shift(model, Z, svecs, epsilon)
    
    t=8
    C = len(pct_shift)
    p = np.empty(C)

    # parameters fit to training data by log regression
    w=torch.tensor([-0.2163,  0.7333,  0.0704,  1.0607,  0.2022, -0.0068,  
                    1.3983,  0.1549, -0.1565,  0.4895,  0.6453,  0.2272,  
                    0.6826,  0.3115,  0.6269])
    b= torch.tensor([-1.98626976])
    
    for i in range(C):
        v = pct_shift[i]
        v[:,i,:] = -1*torch.ones(C,len(epsilon))
        idx = v[:,:,t].argmax()
        row = idx // C
        col = idx % C
        v = v[row,col]
        p[i] = 1 / (1 + torch.exp(-torch.dot(w,v) - b))
    trojan_probability = p.max()
    with open(result_filepath, 'w') as fh:
        fh.write("{}".format(trojan_probability))
        
        
if __name__ == "__main__":
    import argparse
​
    parser = argparse.ArgumentParser(description='Fake Trojan Detector to Demonstrate Test and Evaluation Infrastructure.')
    parser.add_argument('--model_filepath', type=str, help='File path to the pytorch model file to be evaluated.', default='./model.pt')
    parser.add_argument('--result_filepath', type=str, help='File path to the file where output result should be written. After execution this file should contain a single line with a single floating point trojan probability.', default='./output')
    parser.add_argument('--scratch_dirpath', type=str, help='File path to the folder where scratch disk space exists. This folder will be empty at execution start and will be deleted at completion of execution.', default='./scratch')
    parser.add_argument('--examples_dirpath', type=str, help='File path to the folder of examples which might be useful for determining whether a model is poisoned.', default='./example')
​
​
    args = parser.parse_args()
    main(args.model_filepath, args.result_filepath, args.scratch_dirpath, args.examples_dirpath)